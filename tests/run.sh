#!/bin/bash

set -a
driglibash_base=../src/usr/bin/driglibash-base
driglibash_args=../src/usr/bin/driglibash-args
set +a

./driglibash-base.sh
./driglibash-args.sh
