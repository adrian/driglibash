#!/bin/bash


fail () {
	echo -e "$@" >&2
	exit 1
}

here="$(pwd)"
. "$driglibash_base"


###############################################################################
#							here
###############################################################################

# Test from this directory
if [ "$(where)" != "$here" ] ; then
	echo $(where)
	echo $here
	fail "where != here"
fi


###############################################################################
#							yelle
###############################################################################

bash > out 2> err <<-EOF
  . "$driglibash_base"
  yell "coucou"
EOF

if [ "$?" -ne "0" ] ; then fail "'yell' does not exit with status 0" ; fi
if [ "$(cat out)" != "" ] ; then fail "'yell' stdout is not empty (got $(cat out))" ; fi
if [ "$(cat err)" != "coucou" ] ; then fail "'yell' stderr is not correct (got $(cat err))" ; fi


###############################################################################
#							die
###############################################################################

bash > out 2> err <<-EOF
  . "$driglibash_base"
  die "coucou"
EOF

if [ "$?" -ne "1" ] ; then fail "'die' does not exit with status 1" ; fi
if [ "$(cat out)" == "" ] ; then fail "'die' stdout is not empty" ; fi
if [ "$(cat err)" != "coucou" ] ; then fail "'die' stderr is not correct :\n$(cat err | hexdump -c)" ; fi


###############################################################################
#							clean
###############################################################################

clean 'a a a a'
if [ "${driglibash_clean_actions[@]}" != "a a a a" ] ; then fail "clean with only command does not work" ; fi

clean 'b b b'
if [ "${driglibash_clean_actions[0]}" != "a a a a" ] ; then fail "clean append does not work 0" ; fi
if [ "${driglibash_clean_actions[1]}" != "b b b" ] ; then fail "clean append does not work 1" ; fi

clean del 'b b b'
if [ "${driglibash_clean_actions[0]}" != "a a a a" ] ; then fail "clean del does not work 0" ; fi
if [ -n "${driglibash_clean_actions[1]}" ] ; then fail "clean del does not work 1" ; fi

clean post 'b b b'
if [ "${driglibash_clean_actions[0]}" != "a a a a" ] ; then fail "clean post does not work 0 (got '${driglibash_clean_actions[0]}')" ; fi
if [ "${driglibash_clean_actions[1]}" != "b b b" ] ; then fail "clean post does not work 1 (got '${driglibash_clean_actions[0]}')" ; fi

clean pre 'b b b'
if [ "${driglibash_clean_actions[0]}" != "b b b" ] ; then fail "clean pre does not work 0 (got '${driglibash_clean_actions[0]}')" ; fi
if [ "${driglibash_clean_actions[1]}" != "a a a a" ] ; then fail "clean pre does not work 1 (got '${driglibash_clean_actions[1]}')" ; fi
if [ "${driglibash_clean_actions[2]}" != "b b b" ] ; then fail "clean pre does not work 2 (got '${driglibash_clean_actions[2]}')" ; fi



###############################################################################
#							end
###############################################################################
rm out err
echo "OK. PASSED."
